# Fa, fe, fo, fr, fs, ft, fb, fh, fu, fy

Typographic workshop organised in the frame of [erg](http://www.erg.be/m/#Bienvenue_%C3%A0_l%E2%80%99erg) school open doors 2019 with Antoine Gelgon (Luuse, OSP ) et Ludi Loiseau (OSP).

L’atelier propose une pratique en kit de Metapost. Dérivé de Metafont, Metapost est un langage de programmation open source qui permet de dessiner des figures et par extension des polices de caractères. À l’opposé des principaux logiciels de dessins de lettres modélisant celles-ci par leur contours, Metapost décrit la lettre par son “ductus”, c’est-à-dire son chemin central ou squelette. Avec un écho au geste calligraphique, l'outil propose un dessin digital et paramétrique du caractère. L'atelier prendra cette fois la ligature comme sillon, proposant à chacun, individuellement ou collectivement de développer une série de figures, symboles ou caractères liés. En détour par les abréviations médiévales, origine de l'esperluette et variantes contextuelles, nous essayerons différents chemins et fonctions pour lier et délier nos dessins. Liaisons aléatoires et spaghettis pour tous.

jeudi 28 avril: 10h → 18h

vendredi 29 : 9h30 → 17h

salle : 1P06

Apportez vos laptops ou dites nous si vous souhaitez qu'on en emprunte pour vous. Contact : hello@ludi.be

En guise d'introduction → https://vimeo.com/286661261 
