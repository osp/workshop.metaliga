# /usr/bin/python
#/usr/bin/env python2

import fontforge
import glob
import sys
import lxml.etree as ET

FontName = sys.argv[1]
SVG_DIR = glob.glob('svg-over/*.svg')
font = fontforge.open('tmp/empty.sfd')

for glyph in SVG_DIR:
    with open(glyph, 'rt') as f:
        treeLet = ET.parse(f)
    rootLet = treeLet.getroot()
    chasse = rootLet.get('width')

    letter = glyph.split("/")[-1].replace(".svg", "")
    
    svg_file = open(glyph, "r")

    letter_char = font.createChar(int(letter))
    letter_char.importOutlines(glyph)
    letter_char.width = float(chasse)*1.79
    # letter_char.reverseDirection()
    # letter_char.correctDirection()
    print(int(letter))

font.fontname = FontName
font.familyname = 'fontName'
font.generate('final/' + FontName + '.otf')

