
for gly in svg/fin/*.svg;
do
  cp $gly svg-over/
  file=`basename $gly .svg`
  inkscape --verb EditSelectAllInAllLayers \
          --verb SelectionUnGroup \
          --verb ObjectToPath \
          --verb StrokeToPath \
          --verb SelectionUnion \
          --verb SelectionReverse \
          --verb FileSave \
          --verb FileClose \
          --verb FileQuit \
    svg-over/$file.svg \

  echo $file

  # inkscape $gly --export-ps="simple/$file.ps"
  # # inkscape "simple/$file.eps" --export-plain-svg="simple/$file.svg"
  # # rm simple/$file.esp
done

